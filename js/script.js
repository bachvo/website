//looks into the current url and searches for "&" and then goes to search for
//"=" sign and finds the key value pair to return the value
function getQueryVariable(variable){
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){
			return pair[1];}
	}
	return(false);
}

//transforms the string of the value into and integer
var c = parseInt(getQueryVariable("id"));

//puts the integer into the function to rotate the carousel to the correct index
function carouselRotate(x) {
	$(document).ready(function(){
		$('#myCarousel').carousel(x);
	});
}

carouselRotate(c);
