function barChartD3(){

var aspect = 960 / 500,
    chart = $("#chart");
$(window).on("resize", function() {
    var targetWidth = chart.parent().width();
    chart.attr("width", targetWidth);
    chart.attr("height", targetWidth / aspect);
});

var parentX = $('#barContainer').width();
var parentY = $('#barContainer').height();
console.log(parentX);
console.log(parentY);

var n = 4, // number of layers
    m = 58, // number of samples per layer
    stack = d3.layout.stack(),
    layers = stack(d3.range(n).map(function() { return bumpLayer(m, .1); })),
    yGroupMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y; }); }),
    yStackMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); });

var margin = {top: 40, right: 10, bottom: 20, left: 10},
    width = parentX - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .domain(d3.range(m))
    .rangeRoundBands([0, width], .08);

var y = d3.scale.linear()
    .domain([0, yStackMax])
    .range([height, 0]);

var color = d3.scale.linear()
    .domain([0, n - 1])
    .range(["#aad", "#556"]);

var xAxis = d3.svg.axis()
    .scale(x)
    .tickSize(0)
    .tickPadding(6)
    .orient("bottom");

var svg = d3.select("#barChartD3").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var layer = svg.selectAll(".layer")
    .data(layers)
  .enter().append("g")
    .attr("class", "layer")
    .style("fill", function(d, i) { return color(i); });

var rect = layer.selectAll("rect")
    .data(function(d) { return d; })
  .enter().append("rect")
    .attr("x", function(d) { return x(d.x); })
    .attr("y", height)
    .attr("width", x.rangeBand())
    .attr("height", 0);

rect.transition()
    .delay(function(d, i) { return i * 10; })
    .attr("y", function(d) { return y(d.y0 + d.y); })
    .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); });

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

d3.selectAll("input").on("change", change);

var timeout = setTimeout(function() {
  d3.select("input[value=\"grouped\"]").property("checked", true).each(change);
}, 2000);

function change() {
  clearTimeout(timeout);
  if (this.value === "grouped") transitionGrouped();
  else transitionStacked();
}

function transitionGrouped() {
  y.domain([0, yGroupMax]);

  rect.transition()
      .duration(500)
      .delay(function(d, i) { return i * 10; })
      .attr("x", function(d, i, j) { return x(d.x) + x.rangeBand() / n * j; })
      .attr("width", x.rangeBand() / n)
    .transition()
      .attr("y", function(d) { return y(d.y); })
      .attr("height", function(d) { return height - y(d.y); });
}

function transitionStacked() {
  y.domain([0, yStackMax]);

  rect.transition()
      .duration(500)
      .delay(function(d, i) { return i * 10; })
      .attr("y", function(d) { return y(d.y0 + d.y); })
      .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
    .transition()
      .attr("x", function(d) { return x(d.x); })
      .attr("width", x.rangeBand());
}

// Inspired by Lee Byron's test data generator.
function bumpLayer(n, o) {

  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < n; i++) {
      var w = (i / n - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }

  var a = [], i;
  for (i = 0; i < n; ++i) a[i] = o + o * Math.random();
  for (i = 0; i < 5; ++i) bump(a);
  return a.map(function(d, i) { return {x: i, y: Math.max(0, d)}; });
}
};


function circle(){

  var chart = $("#chart"),
    aspect = chart.width() / chart.height(),
    container = chart.parent();
$(window).on("resize", function() {
    var targetWidth = container.width();
    chart.attr("width", targetWidth);
    chart.attr("height", Math.round(targetWidth / aspect));
}).trigger("resize");

};


//Pie Chart --------------------------------------------------------------

function pieChart(){

var chart = c3.generate({
    bindto: '#pieChart',
    size: { height: 180 },
    data: {
        // iris data from R
        columns: [
            ['data1', 30],
            ['data2', 120],
        ],
        type : 'pie',
    },
    pie: {
        onclick: function (d, i) { console.log(d, i); },
        onmouseover: function (d, i) { console.log(d, i); },
        onmouseout: function (d, i) { console.log(d, i); }
    }
});

setTimeout(function () {
    chart.load({
        columns: [
            ["KPI", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            ["Fault Detection", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            ["Energy", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
        ]
    });
}, 1500);

setTimeout(function () {
    chart.unload('data1');
    chart.unload('data2');
}, 2500);

};


//Donut Chart --------------------------------------------------------------

function donutChart(){

var chart = c3.generate({
    bindto: '#donutChart',
    size: { height: 180 },
    data: {
        columns: [
            ["System 1 Power Generated Wh 5min AV", 30],
            ["Solar Radiation Wh/m2 5min AV TL", 120],
        ],
        type : 'donut'
    },
    donut: {
        title: "Solar Radiation",
        onclick: function (d, i) { console.log(d, i); },
        onmouseover: function (d, i) { console.log(d, i); },
        onmouseout: function (d, i) { console.log(d, i); }
    }
});

setTimeout(function () {
    chart.load({
        columns: [
            ["System 1 Power Generated Wh 5min AV", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            ["Solar Radiation Wh/m2 5min AV TL", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            ["System 2 Power Generated Wh 5min AV", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
        ]
    });
}, 1500);

setTimeout(function () {
    chart.unload('data1');
    chart.unload('data2');
}, 2500);
};


//Area Chart --------------------------------------------------------------

function areaChart(){

var chart = c3.generate({
    bindto: '#areaChart',
    size: { width: 900},
    data: {
        json: {
            HeatingFluidRate5minAVTL: [30, 20, 50, 40, 60, 50],
            OutdoorTemp: [200, 130, 90, 240, 130, 220],
            TankTempTopTL: [300, 200, 160, 400, 250, 250]
        },
        types: {
            HeatingFluidRate5minAVTL: 'area-spline',
            OutdoorTemp: 'area-spline',
            TankTempTopTL: 'line',
            // 'line', 'spline', 'step', 'area', 'area-step' are also available to stack
        },
        groups: [['data1', 'data2']]
    }
});

};



//bar Chart --------------------------------------------------------------

function barChart(){

var chart = c3.generate({
    bindto: '#barChart',
    size: { height: 180},
    data: {
        columns: [
            ['JavaScript', 10],
            ['AngularJS', 30]
        ],
        type: 'bar'
    },
    bar: {
        width: {
            ratio: 0.5 // this makes bar width 50% of length between ticks
        }
        // or
        //width: 100 // this makes bar width 100px
    }
});

// setTimeout(function () {
//     chart.load({
//         columns: [
//             ['Energy', 130, 150, 200, 300, 200, 100]
//         ]
//     });
// }, 1000);

};


//combo Chart --------------------------------------------------------------

function comboChart(){

var chart = c3.generate({
    bindto: '#comboChart',
    data: {
        columns: [
            ['Solar Radiation Wh/m2 5min AV TL', 30, 20, 50, 40, 60, 50],
            ['Collector Temp Diff 5min TL', 200, 130, 90, 240, 130, 220],
            ['Tank Temp Top TL', 300, 200, 160, 400, 250, 250],
            ['Collector Return Temp TL', 200, 130, 90, 240, 130, 220],
            ['Solar Fluid Rate 5min AV TL', 130, 120, 150, 140, 160, 150],
            ['Outdoor Temp TL', 90, 70, 20, 50, 60, 120],
        ],
        type: 'bar',
        types: {
            'Solar Radiation Wh/m2 5min AV TL': 'spline',
            'Collector Return Temp TL': 'line',
            'Outdoor Temp TL': 'area',
        },
        groups: [
            ['data1','data2']
        ]
    }
});

};


//gauge Chart --------------------------------------------------------------

function gaugeChart(){

var chart = c3.generate({
    bindto: '#gaugeChart',
    size: { width: 300 },
    data: {
        columns: [
            ['Energy Consumption', 91.4]
        ],
        type: 'gauge'
    },
    gauge: {
//        label: {
//            format: function(value, ratio) {
//                return value;
//            },
//            show: false // to turn off the min/max labels.
//        },
//    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
//    max: 100, // 100 is default
//    units: ' %',
//    width: 39 // for adjusting arc thickness
    },
    color: {
        pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
        threshold: {
//            unit: 'value', // percentage is default
//            max: 200, // 100 is default
            values: [30, 60, 90, 100]
        }
    }
});

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 10]]
    });
}, 1000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 50]]
    });
}, 2000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 70]]
    });
}, 3000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 0]]
    });
}, 4000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 100]]
    });
}, 5000);

};


//step Chart --------------------------------------------------------------

function stepChart() {

  var chart = c3.generate({
    bindto: '#stepChart',
    size: { height: 300 },
    data: {
        columns: [
            ['data1', 300, 350, 300, 0, 0, 100],
            ['data2', 130, 100, 140, 200, 150, 50]
        ],
        types: {
            data1: 'step',
            data2: 'area-step'
        }
    }
});

};




//CLient Charts --------------------------------------------------------------


function comboChartClient(){

var chart = c3.generate({
    bindto: '#comboChartClient',
    size: { width: 800 },
    data: {
        columns: [
            ['KPI', 30, 20, 50, 40, 60, 50],
            ['Fault Detection', 200, 130, 90, 240, 130, 220],
            ['Energy', 300, 200, 160, 400, 250, 250],
            ['Energy KPI', 200, 130, 90, 240, 130, 220],
            ['Infastructure', 130, 120, 150, 140, 160, 150],
            ['Golden Standard', 90, 70, 20, 50, 60, 120],
        ],
        type: 'bar',
        groups: [
            ['data1','data2']
        ]
    }
});

};


//gauge Chart --------------------------------------------------------------

function gaugeChartClient(){

var chart = c3.generate({
    bindto: '#gaugeChartClient',
    size: { width: 300 },
    data: {
        columns: [
            ['Energy Consumption', 91.4]
        ],
        type: 'gauge'
    },
    gauge: {
//        label: {
//            format: function(value, ratio) {
//                return value;
//            },
//            show: false // to turn off the min/max labels.
//        },
//    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
//    max: 100, // 100 is default
//    units: ' %',
//    width: 39 // for adjusting arc thickness
    },
    color: {
        pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
        threshold: {
//            unit: 'value', // percentage is default
//            max: 200, // 100 is default
            values: [30, 60, 90, 100]
        }
    }
});

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 10]]
    });
}, 1000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 50]]
    });
}, 2000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 70]]
    });
}, 3000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 0]]
    });
}, 4000);

setTimeout(function () {
    chart.load({
        columns: [['Energy Consumption', 100]]
    });
}, 5000);

};

//pie Chart --------------------------------------------------------------

function pieChartClient(){

var chart = c3.generate({
    bindto: '#pieChartClient',
    size: { height: 180 },
    data: {
        // iris data from R
        columns: [
            ['data1', 30],
            ['data2', 120],
        ],
        type : 'pie',
    },
    pie: {
        onclick: function (d, i) { console.log(d, i); },
        onmouseover: function (d, i) { console.log(d, i); },
        onmouseout: function (d, i) { console.log(d, i); }
    }
});

setTimeout(function () {
    chart.load({
        columns: [
            ["KPI", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            ["Fault Detection", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            ["Energy", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            ["Energy KPI", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            ["Infastructure", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            ["Golden Standard", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
        ]
    });
}, 1500);

setTimeout(function () {
    chart.unload('data1');
    chart.unload('data2');
}, 2500);

};

//category Chart --------------------------------------------------------------

function catGraph(){

var chart = c3.generate({
    bindto: '#catGraph',
    data: {
        type: 'bar',
        colors: {
            data1: '#0072BB'
        },
        columns: [
            ['data1', 80, 70, 95, 70, 60, 80, 70, 60, 80, 70, 40, 60, 40, 40, 80]
        ],
        names: {
            data1: 'Programming Language'
        }
    },
    axis: {
        x: {
            type: 'category',

            categories: ['JavaScript', 'jQuery', 'HTML5+CSS3', 'AngularJS', 'Web2py', 'Sass+Less', 'Node.js', 'D3.js+C3.js', 'Arduino', 'Java', 'Python', 'Android', 'MongoDB', 'PostgresSQL', 'Git']
        },
        y: {
            label: {
                text: 'Skill Points',
                position: 'outer-middle'
            }
        }
    }
});
};


function catDesignGraph(){

var chart = c3.generate({
    bindto: '#catDesignGraph',
    data: {
        type: 'bar',
        columns: [
            ['data1', 30, 200, 100, 400, 150, 250, 50, 100, 250]
        ]
    },
    axis: {
        x: {
            type: 'category',
            categories: ['JavaScript', 'jQuery', 'HTML5+CSS3', 'AngularJS', 'Web2py', 'Sass+Less', 'Node.js', 'D3.js+C3.js', 'Arduino+Processing', 'Java', 'Python', 'Android Development', 'MongoDB', 'PostgresSQL', 'Git']
        }
    }
});
};