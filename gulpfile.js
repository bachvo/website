var gulp = require('gulp');
var sass = require('gulp-sass');
var log = require('gulp-util').log;
var watch = require('gulp-watch');
var mocha = require('gulp-mocha');
var jshint = require('gulp-jshint');
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var plumber = require('gulp-plumber');
var grep = require('gulp-grep-stream');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var minifyCSS = require('gulp-minify-css');
var imageResize = require('gulp-image-resize');

//paths for each build
var paths = {
  img: 'img/**/*',
  gallery: 'gallery/**/*',
  css: 'css/**/*.css',
  sass: 'sass/**/*.scss',
  js: 'js/**/*.js'
};

//compile sass files
gulp.task('sass', function(){
    gulp.src(paths.sass)
        .pipe(sass({errLogToConsole: true}))
        .pipe(gulp.dest('css'));
});

//minify js code
gulp.task('minify-js', function() {
  gulp.src(paths.js)
    .pipe(uglify(opts))
    .pipe(rename(function (path) {
        path.basename += ".min";
        path.extname = ".js"
    }))
    .pipe(gulp.dest('dist-js'))
});

//thumbnail resizer for img and gallery
gulp.task('img-resize-thumb', function () {
  gulp.src([paths.gallery, paths.img])
    .pipe(imageResize({ 
      width : 390,
      height : 260,
      crop : true,
      upscale : false
    }))
    .pipe(gulp.dest('build-thumbnail'));
});

//thumbnail resizer for img and gallery
gulp.task('img-resize', function () {
  gulp.src([paths.gallery, paths.img])
    .pipe(imageResize({ 
      width : 756,
      height : 504,
      crop : true,
      upscale : false
    }))
    .pipe(gulp.dest('build-img-resize'));
});

//minify images in gallery and img
gulp.task('img-min', function () {
    gulp.src([paths.gallery, paths.img])
        .pipe(imagemin({
          use: [pngquant()]
        }))
        .pipe(gulp.dest('dist-img'));
});

//minify css code
gulp.task('minify-css', function() {
  gulp.src(paths.css)
    .pipe(minifyCSS(opts))
    .pipe(rename(function (path) {
        path.basename += ".min";
        path.extname = ".css"
    }))
    .pipe(gulp.dest('dist-css'))
});

//check js file for errors
gulp.task('lint', function() {
  gulp.src('js/script.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  log('Watching Files');
  	gulp.watch(paths.sass, ['sass']);
  	gulp.watch(paths.css, ['minify-css']);
  	gulp.watch(paths.js, ['minify-js']);
  	gulp.watch('js/script.js', ['lint']);
});

// The default task (called when you run `gulp` from terminal)
gulp.task('default', ['sass', 'lint', 'minify-css','minify-js', 'img-min', 'watch']);

